//
//  mmBusinessObject.h
//  mmiOS
//
//  Created by Kevin McNeish on 11/29/10.
//  Copyright 2011 Oak Leaf Enterprises, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface mmBusinessObject : NSObject {

	NSString *entityClassName;
	NSString *dbName;
	NSManagedObjectContext *managedObjectContext;
    NSManagedObjectModel *managedObjectModel;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@property (nonatomic, copy) NSString* dbName;
@property (nonatomic, copy) NSString* entityClassName;
@property (nonatomic, copy) NSString* pkColumn;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSManagedObject *) createEntity:(NSString *)entityName;
- (NSManagedObject *) createEntity;
- (void) deleteEntity:(NSManagedObject *)entity;
- (NSMutableArray *) getEntities;
- (NSMutableArray *) getEntities: (NSString *)entityName: (NSSortDescriptor *)sortDescriptor: (NSPredicate *)predicate;
- (void)performAutomaticLightweightMigration;
- (BOOL) saveEntities;

@end
