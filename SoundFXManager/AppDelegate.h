//
//  AppDelegate.h
//  SoundFXManager
//
//  Created by Dan Jurden on 11/3/12.
//  Copyright (c) 2012 Dan Jurden. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
