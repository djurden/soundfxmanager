//
//  ABusinessObject.m
//  RosterTest
//
//  Created by Dan Jurden on 9/12/12.
//  Copyright (c) 2012 Dan Jurden. All rights reserved.
//

#import "ABusinessObject.h"

@implementation ABusinessObject

- (id)init {
    if ((self = [super init])) {
        // Custom initialization
        self.dbName = @"iPressBoxx";
    }
    
    return self;
}

- (NSString*) GetGuid{
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    
    // create a new CFStringRef (toll-free bridged to NSString)
    // that you own
    NSString *uuidString = (__bridge NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    
    // release the UUID
    CFRelease(uuid);
    
    return uuidString;
}


@end
