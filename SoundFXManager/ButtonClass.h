//
//  ButtonClass.h
//  SoundFXManager
//
//  Created by Dan Jurden on 11/3/12.
//  Copyright (c) 2012 Dan Jurden. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ButtonClass : NSObject

@property NSNumber *buttonId;
@property NSString *buttonText;
@property NSString *soundFile;

- (id) initWithValues: (NSNumber *)bId: (NSString *)bText: (NSString *)bFile;
@end
