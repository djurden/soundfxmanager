//
//  mmBusinessObject.m
//  mmiOS
//
//  Created by Kevin McNeish on 11/29/10.
//  Copyright 2011 Oak Leaf Enterprises, Inc. All rights reserved.
//

#import "mmBusinessObject.h"

@implementation mmBusinessObject

@synthesize dbName;
@synthesize entityClassName;
@synthesize pkColumn;

// Initialization
- (id)init {
    if ((self = [super init])) {
	
    }
    return self;
}

// Creates a new entity and adds it to the managed object context
- (NSManagedObject *) createEntity:(NSString *)entityName {
	
	return [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:[self managedObjectContext]];
	
}

// Creates a new entity of the default type and adds it to the managed object context
- (NSManagedObject *) createEntity {
	
	return [self createEntity:self.entityClassName];
	
}

// Delete the specified entity
- (void) deleteEntity:(NSManagedObject *)entity {
	[managedObjectContext deleteObject:entity];
}

// Gets entities for the specified request
- (NSMutableArray *) getEntities: (NSString *)entityName: (NSSortDescriptor *)sortDescriptor: (NSPredicate *)predicate {
	
	NSError *error = nil;
	
	// Create the request object
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	// Set the entity type to be fetched
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	// Set the predicate if specified
	if (predicate) {
		[request setPredicate:predicate];
	}
	
	// Set the sort descriptor if specified
	if (sortDescriptor) {
		NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
		[request setSortDescriptors:sortDescriptors];
	}

	// Execute the fetch
	NSMutableArray *mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
	
	if (mutableFetchResults == nil) {
		
		// Handle the error.
	}
	
	return mutableFetchResults;
}

- (NSMutableArray *) getEntities {
	return [self getEntities: self.entityClassName : nil : nil];
}

// Saves all changes (insert, update, delete) of entities
- (BOOL) saveEntities {
	NSError *error = nil;
	BOOL result = [managedObjectContext save:&error];
	
	if (!result) {
		NSLog(@"%@", [error localizedDescription]);
		// Handle the error.
		
	}
	return result;
	
}

#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext {
    
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
	
	managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
	
	return managedObjectModel;
	
	// NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"iPad4000" withExtension:@"momd"];
	// managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];    
    return managedObjectModel;
}

- (void)performAutomaticLightweightMigration {
    
    NSError *error;
	
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@%@", dbName, @".sqlite"]];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil
                                                            URL:storeURL
                                                        options:options 
                                                          error:&error]){
        // Handle the error.
    }
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    
	NSString *dbcName = dbName;
	
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@%@", dbcName, @".sqlite"]];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        if ([error code] == 134100) {
            [self performAutomaticLightweightMigration];
                if ([persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
                   return persistentStoreCoordinator;
                }
        }
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return persistentStoreCoordinator;
}


#pragma mark -
#pragma mark Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark -
#pragma mark Memory management


@end
