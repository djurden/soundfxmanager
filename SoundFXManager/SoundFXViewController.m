//
//  SoundFXViewController.m
//  SoundFXManager
//
//  Created by Dan Jurden on 11/3/12.
//  Copyright (c) 2012 Dan Jurden. All rights reserved.
//

#import "SoundFXViewController.h"
#import "ButtonClass.h"

@interface SoundFXViewController ()

@end

@implementation SoundFXViewController

NSString *currentDevice;
NSString *orientation;
int buttonRow;
int buttonColumn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        currentDevice = @"iPad";
    }
    else {
        currentDevice = @"iPhone";
    }
    
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        orientation = @"L";
    }
    else {
        orientation = @"P";
    }

    [self ConfigureButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    if (UIInterfaceOrientationIsLandscape(fromInterfaceOrientation)) {
        orientation = @"P";
    }
    else {
        orientation = @"L";
    }
    
    [self ConfigureButtons];
}

- (void) ConfigureButtons {
    NSMutableArray *buttonArray = [self GetButtons];
    
    NSArray *viewsToRemove = [self.view subviews];
    
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    buttonRow = 0;
    buttonColumn = 0;
    
    for (ButtonClass* button in buttonArray) {
        [self.view addSubview:[self CreateButton:button]];
    }
    
}

- (NSMutableArray *) GetButtons {
    NSMutableArray *buttonArray = [[NSMutableArray alloc] init];
    int buttonCount;
    
    if ([currentDevice isEqualToString:@"iPhone"]) {
        buttonCount = 30;
    }
    else {
        buttonCount = 60;
    }
    
    for (int i = 1; i <= buttonCount; i++) {
        [buttonArray addObject:[[ButtonClass alloc] initWithValues:[[NSNumber alloc] initWithInt:i]:[[NSString alloc] initWithFormat:@"Button %d", i] :@""]];
    }
         
    return buttonArray;
}

- (UIButton*) CreateButton:(ButtonClass *)buttonClass {
    
    int buttonsInColumn;
    CGFloat height;
    CGFloat width;
    CGFloat verticalBuffer;
    CGFloat horizontalBuffer;
    CGFloat startTop;
    CGFloat startLeft;
    
    if ([currentDevice isEqualToString:@"iPad"]) {
        
        if ([orientation isEqualToString:@"P"]) {
            buttonsInColumn = 15;
            startTop = 30;
            startLeft = 20;
            height = 44;
            verticalBuffer = 15;
            horizontalBuffer = 25;
            width = 160;
        }
        else {
            buttonsInColumn = 12;
            startTop = 20;
            startLeft = 20;
            height = 44;
            verticalBuffer = 10;
            horizontalBuffer = 30;
            width = 170;
            
        }
    }
    else {
        
        if ([orientation isEqualToString:@"P"]) {
            buttonsInColumn = 10;
            startTop = 10;
            startLeft = 10;
            height = 34;
            verticalBuffer = 6;
            horizontalBuffer = 10;
            width = 93;
        }
        else {
            buttonsInColumn = 6;
            startTop = 10;
            startLeft = 10;
            height = 34;
            verticalBuffer = 6;
            horizontalBuffer = 10;
            width = 84;
        }
    }
    
    //int col = buttonNumber / buttonsInColumn;
    //int row = (buttonNumber % buttonsInColumn) - 1;
    
    CGFloat y = buttonRow * (height + verticalBuffer) + startTop ;
    CGFloat x = buttonColumn * (width + horizontalBuffer) + startLeft;
    
    CGRect rect = CGRectMake(x, y, width, height);
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonClicked:)
     forControlEvents:UIControlEventTouchUpInside];
    
    button.tag = [buttonClass.buttonId intValue];
    [button setFrame:rect];
    [button setTitle:buttonClass.buttonText forState:UIControlStateNormal];
    
    if (buttonRow++ == buttonsInColumn - 1) {
        buttonRow = 0;
        buttonColumn++;
    }
    
    return button;
}

- (void) buttonClicked:(UIButton*)sender {
    
    NSString *alertMessage = [[NSString alloc] initWithFormat:@"Button %d clicked", sender.tag];
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Button"
                          message:alertMessage
                          delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil];
    
    [alert show];
    
}

@end
