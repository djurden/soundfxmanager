//
//  ButtonClass.m
//  SoundFXManager
//
//  Created by Dan Jurden on 11/3/12.
//  Copyright (c) 2012 Dan Jurden. All rights reserved.
//

#import "ButtonClass.h"

@implementation ButtonClass

- (id) initWithValues:(NSNumber *)bId :(NSString *)bText :(NSString *)bFile {
    self = [super init];
    
    self.buttonId = bId;
    self.buttonText = bText;
    self.soundFile = bFile;
    
    return self;
}

@end
