//
//  ButtonEntity.m
//  SoundFXManager
//
//  Created by Dan Jurden on 11/4/12.
//  Copyright (c) 2012 Dan Jurden. All rights reserved.
//

#import "ButtonEntity.h"


@implementation ButtonEntity

@dynamic buttonId;
@dynamic buttonText;
@dynamic attachedSound;
@dynamic active;

@end
