//
//  main.m
//  SoundFXManager
//
//  Created by Dan Jurden on 11/3/12.
//  Copyright (c) 2012 Dan Jurden. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
