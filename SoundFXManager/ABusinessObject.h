//
//  ABusinessObject.h
//  RosterTest
//
//  Created by Dan Jurden on 9/12/12.
//  Copyright (c) 2012 Dan Jurden. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mmBusinessObject.h"

@interface ABusinessObject : mmBusinessObject

-(NSString *) GetGuid;

@end
