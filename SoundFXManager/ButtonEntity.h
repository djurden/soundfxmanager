//
//  ButtonEntity.h
//  SoundFXManager
//
//  Created by Dan Jurden on 11/4/12.
//  Copyright (c) 2012 Dan Jurden. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ButtonEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * buttonId;
@property (nonatomic, retain) NSString * buttonText;
@property (nonatomic, retain) NSString * attachedSound;
@property (nonatomic, retain) NSNumber * active;

@end
